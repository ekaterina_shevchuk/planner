import React from "react";
import { configure, shallow, render } from 'enzyme';
import Header from './Header';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering header', () => {
    const wrapper = render(<Header />);
    expect(wrapper.find('div')).toHaveLength(1);
  });
});