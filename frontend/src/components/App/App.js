import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';
import Header from  "../Header/Header";
import TaskInputForm from "../TaskInputForm/TaskInputForm";
import Router from '../Router/Router';
import ShedulePage from "../ShedulePage/ShedulePage";

function App() {
  return (
    <div className="App">
      <Header/>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Router}/>
          <Route exact path='/shedule-page' component={ShedulePage}/>
          <Route exact path='/add-task' component={TaskInputForm}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
