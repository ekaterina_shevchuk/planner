import React from 'react';
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import './Router.css';

export default function RouterMisha() {
  return(
    <div className="Router">

      <Card className="card">
        <CardActionArea>
          <CardContent>
            <Link to="/shedule-page">
              <CardMedia
                className="media"
                image="./images/cards/calendar.png"
                title="Contemplative Reptile"
              />
            </Link>
          </CardContent>
        </CardActionArea>
        <div className="card-title">
          <Typography variant="h5" align="center" color="primary" >
            Расписание
          </Typography>
        </div>
      </Card>

      <Card className="card">
        <CardActionArea>
          <CardContent>
            <Link to="/add-task">
              <CardMedia
                className="media"
                image="./images/cards/technology.png"
                title="Contemplative Reptile"
              />
            </Link>
          </CardContent>
        </CardActionArea>
        <Typography variant="h5" align="center" color="primary">
          Добавить задание
        </Typography>
      </Card>
    </div>
  );
}