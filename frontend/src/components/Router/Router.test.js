import React from "react";
import { configure, shallow } from 'enzyme';
import Router from './Router';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering router', () => {
    const wrapper = shallow(<Router />);
    expect(wrapper.find('div')).toHaveLength(2);
  });
});