import React from 'react';
import Shedule from "../Shedule/Shedule";
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import './SheduleWrapper.css';

export default class SheduleWrapper extends React.PureComponent {

  datesStorage = [
    '2020-01-01',
    '2020-02-01',
    '2020-03-01',
    '2020-04-01',
    '2020-05-01',
    '2020-06-01',
    '2020-07-01',
    '2020-08-01',
    '2020-09-01',
    '2020-10-01',
    '2020-11-01',
    '2020-12-01',
  ];

  constructor(props) {
    super(props);

    const today = new Date();
    const currentDateTmp = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    this.state = {
      currentDate: currentDateTmp,
      currentMonth: today.getMonth(),
    };

    this.leftClick = this.leftClick.bind(this);
    this.rightClick = this.rightClick.bind(this);
  }

  leftClick() {
    const index = this.state.currentMonth;
    if (index > 0) {
      this.setState(state => ({
        currentDate: this.datesStorage[index-1],
        currentMonth: index-1,
      }));
    }
  };

  rightClick() {
    const index = this.state.currentMonth;
    if (index < 11) {
      this.setState(state => ({
        currentDate: this.datesStorage[index+1],
        currentMonth: index+1,
      }));
    }
  };

  chooseMonth(index) {
    switch (index) {
      case 0: return "January";
      case 1: return "February";
      case 2: return "March";
      case 3: return "April";
      case 4: return "May";
      case 5: return "June";
      case 6: return "July";
      case 7: return "August";
      case 8: return "September";
      case 9: return "October";
      case 10: return "November";
      case 11: return "December";
    }
  }

  render() {
    const innerStyle = {
      width: (12 * 920) + "px",
      left: (this.state.currentMonth * 920 * -1) + "px"
    };

    return (
      <div className="SheduleWrapper">
        <div className="slider__header">
          <IconButton onClick={this.leftClick}>
            <ArrowBackIosIcon />
          </IconButton>

          <p>{this.chooseMonth(this.state.currentMonth)}</p>

          <IconButton onClick={this.rightClick}>
            <ArrowForwardIosIcon />
          </IconButton>
        </div>

        <div className="simple-slider__slider-outer">
          <div className="simple-slider__slider-inner" style={innerStyle}>
            {this.datesStorage.map(el => <Shedule currentDate={el} key={el}/>)}
          </div>
        </div>
      </div>
    );
  }
}