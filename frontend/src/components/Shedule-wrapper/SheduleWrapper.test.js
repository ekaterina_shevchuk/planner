import React from "react";
import { configure, shallow } from 'enzyme';
import SheduleWrapper from './SheduleWrapper';
import Adapter from "enzyme-adapter-react-16";
import {emptyData} from "../../entities/month-tasks";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering sheduleWrapper', () => {
    const wrapper = shallow(<SheduleWrapper />);
    const today = new Date();
    const currentDateTmp = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    expect(wrapper.find('div')).toHaveLength(4);
    expect(wrapper.instance().state.currentDate).toEqual(currentDateTmp);
    expect(wrapper.instance().state.currentMonth).toEqual(today.getMonth());
  });

  it('choose month', () => {
    const wrapper = shallow(<SheduleWrapper />);

    let result = wrapper.instance().chooseMonth(0);
    let expectRes = "January";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(1);
    expectRes = "February";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(2);
    expectRes = "March";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(3);
    expectRes = "April";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(4);
    expectRes = "May";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(5);
    expectRes = "June";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(6);
    expectRes = "July";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(7);
    expectRes = "August";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(8);
    expectRes = "September";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(9);
    expectRes = "October";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(10);
    expectRes = "November";
    expect(result).toEqual(expectRes);

    result = wrapper.instance().chooseMonth(11);
    expectRes = "December";
    expect(result).toEqual(expectRes);
  });

});