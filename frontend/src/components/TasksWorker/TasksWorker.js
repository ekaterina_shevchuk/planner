import * as React from "react";

export default class TasksWorker {

  convertTasks(backendTasks) {
    let frontendTasks = [];
    backendTasks.forEach((task) => {
      frontendTasks.push({
        id: task.id,
        title: task.title,
        startDate: this.convertDate(task.caseDate, 1),
        endDate: this.convertDate(task.caseDate, 2),
        description: task.descriptionCase
      });
    });
    return frontendTasks;
  }

  convertDate(stringDate, hours) {
    const arrayOfDate = stringDate.split('-');
    return new Date(
      arrayOfDate[0],
      arrayOfDate[1]-1,
      arrayOfDate[2].slice(0,2),
      hours,
      0
    );
  }
}
