import React from "react";
import { configure, shallow, render } from 'enzyme';
import TaskInputForm from './TaskInputForm';
import Adapter from "enzyme-adapter-react-16";
import axios from 'axios';

jest.mock('axios');
configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering taskInputForm', () => {
    const wrapper = shallow(<TaskInputForm />);
    expect(wrapper.find('div')).toHaveLength(4);
    expect(wrapper.instance().state.title).toEqual('default');
    expect(wrapper.instance().state.description).toEqual('default');
    expect(wrapper.instance().state.currentDate).toEqual('2020 Jun 03');
  });

  it('preparing task', () => {
    const wrapper = shallow(<TaskInputForm />);
    const result = wrapper.instance().prepareTask();
    const expectRes = {
      idU: 1,
      d: 'default',
      date: '2020-06-03',
      title: 'default'
    };
    expect(result).toEqual(expectRes);
  });

  it('updating data', () => {
    const wrapper = shallow(<TaskInputForm />);
    const result = wrapper.instance().updateData('2020-06-10');
    expect(wrapper.instance().state.currentDate).toEqual('2020-06-10');
  });
});
