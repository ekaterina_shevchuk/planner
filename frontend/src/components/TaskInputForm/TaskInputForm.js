import * as React from "react";
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import DatePicker from "./DatePicker/DatePicker";
import './TaskInputForm.css';
import api from "../../api";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';


export default class TaskInputForm extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      title: 'default',
      description: 'default',
      currentDate: '2020 Jun 03'
    };
  }

  updateData = (value) => {
    this.setState({ currentDate: value });
  };

  onSubmit = async () => {
    const response = await api.post('/save', null, {params: this.prepareTask()});
    window.location.reload();
  };

  prepareTask() {
    const cDate = this.state.currentDate.toString();
    // console.log(cDate);
    const arrayDate = cDate.split(' ');
    let preparedDate = '2020-';

    switch (arrayDate[1]) {
      case 'Jan':
        preparedDate = preparedDate + '01-';
        break;
      case 'Feb':
        preparedDate = preparedDate + '02-';
        break;
      case 'Mar':
        preparedDate = preparedDate + '03-';
        break;
      case 'Apr':
        preparedDate = preparedDate + '04-';
        break;
      case 'May':
        preparedDate = preparedDate + '05-';
        break;
      case 'Jun':
        preparedDate = preparedDate + '06-';
        break;
      case 'Jul':
        preparedDate = preparedDate + '07-';
        break;
      case 'Aug':
        preparedDate = preparedDate + '08-';
        break;
      case 'Sep':
        preparedDate = preparedDate + '09-';
        break;
      case 'Oct':
        preparedDate = preparedDate + '10-';
        break;
      case 'Nov':
        preparedDate = preparedDate + '11-';
        break;
      case 'Dec':
        preparedDate = preparedDate + '12-';
        break;
    }

    preparedDate = preparedDate + arrayDate[2];

    return {
      idU: 1,
      d: this.state.description.toString(),
      date: preparedDate.toString(),
      title: this.state.title.toString()
    };
  }

  setTaskTitle = (event) => {
    this.setState({title: event.target.value});
  };

  setTaskDescription = (event) => {
    this.setState({description: event.target.value});
  };

  render() {
    return (
      <div className="TaskInputForm">
        <div className="close-button">
          <Link to="/">
            <IconButton color="primary">
              <CloseIcon/>
            </IconButton>
          </Link>
        </div>
        <div className="task_form">
          <TextField id="standard-basic" label="Title" onChange={this.setTaskTitle} />
          <TextField
            id="outlined-multiline-static"
            label="Description"
            multiline
            rows={4}
            defaultValue="no description"
            variant="outlined"
            onChange={this.setTaskDescription}
          />
        </div>
        <DatePicker updateData={this.updateData} />
        <div className="submit-button">
          <Button variant="contained" color="primary" onClick={this.onSubmit}>Submit</Button>
        </div>
      </div>
    );
  }
}