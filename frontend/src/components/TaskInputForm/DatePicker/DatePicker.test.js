import React from "react";
import { configure, shallow } from 'enzyme';
import DatePicker from './DatePicker';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering datePicker', () => {
    const wrapper = shallow(<DatePicker />);
    expect(wrapper.find('div')).toHaveLength(1);
  });
});