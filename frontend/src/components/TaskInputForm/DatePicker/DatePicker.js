import 'date-fns';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import './DatePicker.css';

export default function MaterialUIPickers(props) {
  const today = new Date();
  const currentDateTmp = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  const [selectedDate, setSelectedDate] = React.useState(new Date(currentDateTmp));

  const handleDateChange = (date) => {
    setSelectedDate(date);
    props.updateData(date);
  };

  return (
    <div className="DatePicker">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container justify="space-between">
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="Choose date"
            value={selectedDate}
            onChange={handleDateChange}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        </Grid>
      </MuiPickersUtilsProvider>
    </div>
  );
}