import React from "react";
import { configure, shallow } from 'enzyme';
import TasksList from './TasksList';
import Adapter from "enzyme-adapter-react-16";
import {emptyData} from "../../entities/month-tasks";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering tasksList', () => {
    const wrapper = shallow(<TasksList />);
    expect(wrapper.find('div')).toHaveLength(1);
    expect(wrapper.instance().state.data).toEqual(emptyData);
  });
});