import * as React from "react";
import { emptyData } from "../../entities/month-tasks";
import api from "../../api";
import TasksWorker from "../TasksWorker/TasksWorker";
import './TaskList.css';
import Card from '@material-ui/core/Card';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

export default class TasksList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: emptyData,
      currentDate: this.props.currentDate,
    };
  }

  componentDidMount = async event => {
    const tasksWorker = new TasksWorker();
    await api.get("getAll")
      .then(res => {
        const tasks = tasksWorker.convertTasks(res.data);
        this.setState({data: tasks});
      })
  };

  onSubmit = async (taskId) => {
    const response = await api.post('/delete', null, {params: {taskId: taskId}});
    // console.log(response);
    const currentTasks = this.state.data.filter(task => task.id !== taskId);
    this.setState({data: currentTasks})
  };

  render() {
    const {data} = this.state;

    return (
      <div className="TasksList">
        {
          data.map(task =>
            <div key={task.id}>
              <Card className="task-card">
                <div className="spoiler">
                  <div className="card-header">
                    <Typography variant="h6">
                      {task.title}
                    </Typography>
                  </div>
                  <input type="checkbox"/>
                  <div className="box">
                    <p>{task.description}</p>
                    <IconButton aria-label="delete" onClick={ () => this.onSubmit(task.id)}>
                      <DeleteIcon />
                    </IconButton>
                  </div>
                </div>
              </Card>
            </div>
          )
        }
      </div>
    );
  }
}
