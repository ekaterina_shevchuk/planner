import React from "react";
import Shedule from './Shedule';
import { emptyData, testData } from '../../entities/month-tasks';
import { shallow, configure, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});
jest.mock("react-cookies");

const axios = require("axios");
var MockAdapter = require("axios-mock-adapter");
var mock = new MockAdapter(axios);

describe('form', () => {

  beforeAll(() => {
    mock.onGet("/getAll").reply(200, testData);
  });

  it('rendering shedule', () => {
    const wrapper = shallow(<Shedule />);
    expect(wrapper.find('div')).toHaveLength(1);
    expect(wrapper.instance().state.data).toEqual(emptyData);
  });

  // it('getting request', () => {
  //   const wrapper = render(<Shedule />);
  //   const a = wrapper.instance().componentDidMount;
  //   expect(wrapper.instance().componentDidMount).toBe("au");
  // });

});