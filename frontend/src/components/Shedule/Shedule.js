import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
    Scheduler,
    MonthView,
    Appointments,
} from '@devexpress/dx-react-scheduler-material-ui';
import { emptyData } from '../../entities/month-tasks';
import './Shedule.css';
import api from "../../api";
import TasksWorker from "../TasksWorker/TasksWorker";

export default class Shedule extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: emptyData,
            currentDate: this.props.currentDate,
        };
    }

    componentDidMount = async event => {
        const tasksWorker = new TasksWorker();
        await api.get("getAll")
          .then(res => {
              const tasks = tasksWorker.convertTasks(res.data);
              this.setState({data: tasks});
          })
    };

    render() {
        const {data, currentDate} = this.state;

        return (
          <div className="Shedule">
            <Paper>
                <Scheduler
                    data={data}
                >
                    <ViewState
                        currentDate={currentDate}
                    />
                    <MonthView
                        intervalCount={1}
                    />
                    <Appointments />
                </Scheduler>
            </Paper>
          </div>
        );
    }
}
