import React from "react";
import { configure, shallow } from 'enzyme';
import ShedulePage from './ShedulePage';
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});
describe('form', () => {

  it('rendering sheduleWrapper', () => {
    const wrapper = shallow(<ShedulePage />);
    expect(wrapper.find('div')).toHaveLength(2);
  });
});