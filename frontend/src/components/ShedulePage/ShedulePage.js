import React from "react";
import SheduleWrapper from "../Shedule-wrapper/SheduleWrapper";
import TasksList from "../Tasks-list/TasksList";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
import'./ShedulePage.css';

export default class ShedulePage extends React.PureComponent {
  render() {
    return(
      <div className="ShedulePage">
        <div className="close-button">
          <Link to="/">
            <IconButton color="primary">
              <CloseIcon/>
            </IconButton>
          </Link>
        </div>
        <SheduleWrapper />
        <TasksList/>
      </div>
    );
  }

}