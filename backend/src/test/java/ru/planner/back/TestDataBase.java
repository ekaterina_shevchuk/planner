package ru.planner.back;

import org.dbunit.Assertion;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.planner.back.config.DBUnit;
import ru.planner.back.domain.*;
import java.util.Date;

import java.io.IOException;

@SpringBootTest
public class TestDataBase extends DBUnit {

    @Autowired
    private DataBase dataBase;

    public TestDataBase() throws IOException {
        super(DataBase.class.getName());
    }

    @BeforeEach
    public void init() throws Exception {
        super.init();
        beforeData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("db.unit/DataBaseTest.xml"));
        tester.setDataSet(beforeData);
        tester.onSetup();
    }

    @Test
    void getAll() throws Exception {
        ITable expectedData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("db.unit/DataBaseTest_getAll.xml"))
                .getTable("list_of_cases");
        ITable actualData = tester.getConnection().createDataSet().getTable("list_of_cases");
        Column[] expectedColumns = expectedData.getTableMetaData().getColumns();
        ITable sortedActual = new SortedTable(actualData, expectedColumns);
        Assertion.assertEquals(expectedData, sortedActual);
    }

    
    @Test
    void save() throws Exception {
        Date testDate = new Date(0);
        Case newCase = new Case(1, 1, testDate, "description1", "title1");
        dataBase.saveCase(newCase);

        ITable expectedTable = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("db.unit/DataBaseTest_saveCase.xml"))
                .getTable("list_of_cases");
        ITable actualTable = tester.getConnection().createDataSet().getTable("list_of_cases");
        Column[] expectedColumns = expectedTable.getTableMetaData().getColumns();
        ITable sortedActual = new SortedTable(actualTable, expectedColumns);
        Assertion.assertEquals(expectedTable, sortedActual);
    }

       @Test
    void delete() throws Exception {
        dataBase.deleteCase(3);

        ITable expectedTable = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("db.unit/DataBaseTest_deleteCase.xml"))
                .getTable("list_of_cases");
        ITable actualTable = tester.getConnection().createDataSet().getTable("list_of_cases");
        Column[] expectedColumns = expectedTable.getTableMetaData().getColumns();
        ITable sortedActual = new SortedTable(actualTable, expectedColumns);
        Assertion.assertEquals(expectedTable, sortedActual);
    }

}
