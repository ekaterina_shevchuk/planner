package ru.planner.back.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.lang.*;

public class UserTest {

    String dateInString = "10.10.1990";
    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    Date date;

    public Date parseStringToDate(String dateInString) {
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException ex){
        }
        return date;
    }

    User user = new User(1, "Ivan", parseStringToDate(dateInString), "male");

    @Test
    void testGetId_user() {
        Assertions.assertEquals(1, user.getIdUser());
    }

    @Test
    void testGetFirst_name() {
        Assertions.assertEquals("Ivan", user.getFirstName());
    }

    @Test
    void testGetBirthDate() {
        Assertions.assertEquals("10.10.1990", formatter.format(user.getBirthDate()));
    }

    @Test
    void testGetGender() {
        Assertions.assertEquals("male", user.getGender());
    }

    @Test
    void testSetId_user() throws NoSuchFieldException, IllegalAccessException{
        user.setIdUser(2);
        final Field field = user.getClass().getDeclaredField("idUser");
        field.setAccessible(true);
        Assertions.assertEquals(2, field.get(user));
    }

    @Test
    void testSetFirst_name() throws NoSuchFieldException, IllegalAccessException{
        user.setFirstName("Kate");
        final Field field = user.getClass().getDeclaredField("firstName");
        field.setAccessible(true);
        Assertions.assertEquals("Kate", field.get(user));
    }

    @Test
    void testSetBirthDate() throws NoSuchFieldException, IllegalAccessException{
        user.setBirthDate(parseStringToDate("10.10.2000"));
        final Field field = user.getClass().getDeclaredField("birthDate");
        field.setAccessible(true);
        Assertions.assertEquals("10.10.2000", formatter.format(field.get(user)));
    }

    @Test
    void testSetGender() throws NoSuchFieldException, IllegalAccessException{
        user.setGender("female");
        final Field field = user.getClass().getDeclaredField("gender");
        field.setAccessible(true);
        Assertions.assertEquals("female", field.get(user));
    }

}
