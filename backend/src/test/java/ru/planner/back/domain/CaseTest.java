package ru.planner.back.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.lang.*;


public class CaseTest {
    String dateInString = "22.05.2020";
    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    Date date;

    public Date parseStringToDate(String dateInString) {
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException ex){
        }
        return date;
    }

    Case caseTest = new Case(1, 2, parseStringToDate(dateInString), "Simple Description", "Simple title");

    @Test
    void testGetId_case() {
        Assertions.assertEquals(1, caseTest.getId());
    }

    @Test
    void testGetId_users() {
        Assertions.assertEquals(2, caseTest.getUserId());
    }

    @Test
    void testGetCaseDate() {
        Assertions.assertEquals("22.05.2020", formatter.format(caseTest.getCaseDate()));
    }

    @Test
    void testGetDescriptionCase() {
        Assertions.assertEquals("Simple Description", caseTest.getDescriptionCase());
    }

    @Test
    void testGetTitle() {
        Assertions.assertEquals("Simple title", caseTest.getTitle());
    }

    @Test
    void testSetId_case() throws NoSuchFieldException, IllegalAccessException {
        caseTest.setId(3);
        final Field field = caseTest.getClass().getDeclaredField("id");
        field.setAccessible(true);
        Assertions.assertEquals(3, field.get(caseTest));
    }

    @Test
    void testSetId_users() throws NoSuchFieldException, IllegalAccessException {
        caseTest.setUserId(4);
        final Field field = caseTest.getClass().getDeclaredField("userId");
        field.setAccessible(true);
        Assertions.assertEquals(4, field.get(caseTest));
    }

    @Test
    void testSetcaseDate() throws NoSuchFieldException, IllegalAccessException {
        caseTest.setCaseDate(parseStringToDate("10.10.2001"));
        final Field field = caseTest.getClass().getDeclaredField("caseDate");
        field.setAccessible(true);
        Assertions.assertEquals("10.10.2001", formatter.format(field.get(caseTest)));
    }

    @Test
    void testSetDescriptionCase() throws NoSuchFieldException, IllegalAccessException {
        caseTest.setDescriptionCase("Test Description");
        final Field field = caseTest.getClass().getDeclaredField("descriptionCase");
        field.setAccessible(true);
        Assertions.assertEquals("Test Description", field.get(caseTest));
    }

    @Test
    void testSetTitle() throws NoSuchFieldException, IllegalAccessException {
        caseTest.setTitle("Test Tittle");
        final Field field = caseTest.getClass().getDeclaredField("title");
        field.setAccessible(true);
        Assertions.assertEquals("Test Tittle", field.get(caseTest));
    }

}
