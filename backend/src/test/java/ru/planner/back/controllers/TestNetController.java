package ru.planner.back.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.planner.back.DataBase;
import ru.planner.back.domain.Case;
import ru.planner.back.repos.CaseRepository;

import java.sql.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class TestNetController {

    private NetController netController;
    private CaseRepository caseRepository;
    private DataBase dataBase ;
    private List<Case> expectedCase;
    private ResponseEntity<List<Case>> listResponseEntity;

    @BeforeEach
    void setUp(){
        caseRepository = mock(CaseRepository.class);
        dataBase = spy(DataBase.class);
        dataBase.setCaseRepository(caseRepository);
        netController = new NetController(dataBase);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void save(){
        String expected_answ = "Save success";
        Mockito.when(netController.saveListOfCases(1, "d", "2020-05-05", "t")).thenReturn(expected_answ);
        Assertions.assertEquals(expected_answ, netController.saveListOfCases(1, "d", "2020-05-05", "t"));
    }

    @Test
    void updateListOfCasesTitle1(){
        String expected_answ = "old title is empty";
        Assertions.assertEquals(expected_answ, netController.updateListOfCasesTitle("", "2020-05-05", "nt"));
    }

    @Test
    void updateListOfCasesTitle(){
        String expected_answ = "alter ok";
        Assertions.assertEquals(expected_answ, netController.updateListOfCasesTitle("t", "2020-05-05", "nt"));
    }

    @Test
    void updateListOfCases1(){
        String expected_answ = "old description is empty";
        Assertions.assertEquals(expected_answ, netController.updateListOfCases("", "2020-05-05", "nd"));
    }

    @Test
    void updateListOfCases2(){
        String expected_answ = "alter ok";
        Assertions.assertEquals(expected_answ, netController.updateListOfCases("d", "2020-05-05", "nd"));
    }

    @Test
    void deleteListOfCases(){
        Assertions.assertEquals( new ResponseEntity<>(HttpStatus.NO_CONTENT), netController.deleteListOfCases(1));
        Mockito.verify(dataBase, Mockito.times(1)).deleteCase(1);
    }

    @Test
    void getAll(){
        expectedCase = null;
        listResponseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        Mockito.when(netController.getAllListOfCases()).thenReturn((ResponseEntity<List<Case>>) expectedCase);
        Assertions.assertEquals(listResponseEntity, netController.getAllListOfCases());
    }

}
