package ru.planner.back;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import ru.planner.back.domain.Case;
import ru.planner.back.repos.CaseRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;


public class TestDataBase2 {
  
    private CaseRepository caseRepository;
    private DataBase dataBase ;
    private List<Case> expectedCase;

 
    @BeforeEach
    void setUp(){
        caseRepository = mock(CaseRepository.class);
        dataBase = new DataBase(caseRepository);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAll(){
        expectedCase = null;
        Mockito.when(dataBase.getAll()).thenReturn(expectedCase);
        Assertions.assertEquals(expectedCase, dataBase.getAll());
    }

    @Test
    void saveCase() {
        Case case1 = new Case(1, new Date(2020, 05, 27), "desc", "title");
        Assertions.assertDoesNotThrow(() -> dataBase.saveCase(case1));
        Mockito.verify(caseRepository, Mockito.times(1)).save(case1);
    }

      @Test
    void alterCase(){
        Case case1 = new Case(1, new Date(2020, 05, 27), "desc", "title");
        dataBase.alterCase("text", "2020-05-31", "new desc");
        Mockito.verify(caseRepository, Mockito.times(1)).findByDescriptionCaseAndCaseDate("text", Date.valueOf("2020-05-31"));
    }

    @Test
    void alterCaseTitle(){
        Case case1 = new Case(1, new Date(2020, 05, 27), "desc", "title");
        dataBase.alterCaseTitle("text", "2020-05-31", "new title");
        Mockito.verify(caseRepository, Mockito.times(1)).findByDescriptionCaseAndCaseDate("text", Date.valueOf("2020-05-31"));
    }

    @Test
    void deleteCase(){
        dataBase.deleteCase(1);
        Mockito.verify(caseRepository, Mockito.times(1)).deleteById(1);
    }

}
