package ru.planner.back.config;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.junit.Before;

import java.io.IOException;
import java.util.Properties;

public class DBUnit extends DBTestCase {
    protected IDatabaseTester tester;
    protected IDataSet beforeData;
    private final Properties prop = new Properties();

    @Before
    public void init() throws Exception {
        tester = new JdbcDatabaseTester(prop.getProperty("db.driverClassName"),
                prop.getProperty("db.url"), prop.getProperty("db.username"), prop.getProperty("db.password"));
    }

    public DBUnit(String name) throws IOException {
        super(name);
        prop.load(Thread.currentThread()
                .getContextClassLoader().getResourceAsStream("db.config.properties"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, prop.getProperty("db.driverClassName"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, prop.getProperty("db.url"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, prop.getProperty("db.username"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, prop.getProperty("db.password"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "DB_Unit");
    }

    @Override
    protected IDataSet getDataSet() {
        return beforeData;
    }
}
