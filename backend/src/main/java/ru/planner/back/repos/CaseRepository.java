package ru.planner.back.repos;

import ru.planner.back.domain.Case;
import org.springframework.data.repository.CrudRepository;
import java.sql.Date;
import java.util.List;

public interface CaseRepository extends CrudRepository<Case, Integer> {
    List<Case> findAll();
    Case findByDescriptionCaseAndCaseDate(String descriptionCase, Date caseDate);
}
