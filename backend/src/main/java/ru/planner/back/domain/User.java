package ru.planner.back.domain;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "users")
@NamedQuery(name = "User.getAll", query = "select u from User u")
public class User {
    @Id
    @GeneratedValue
    private int idUser;
    private String firstName;
    private Date birthDate;
    private String gender;

    public User(){
    }

    public User(int idUser, String firstName, Date birthDate, String gender){
        this.idUser = idUser;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) { this.birthDate = birthDate; }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}

