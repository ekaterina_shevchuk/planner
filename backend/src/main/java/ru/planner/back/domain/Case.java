package ru.planner.back.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "list_of_cases")
public class Case {
    @Id
    @GeneratedValue
    private int id;
    private int userId;
    private Date caseDate;
    private String descriptionCase;
    private String title;

    public Case(){
    }

    public Case(int id, int userId, Date caseDate, String descriptionCase, String title){
        this.id = id;
        this.userId = userId;
        this.caseDate = caseDate;
        this.title = title;
        this.descriptionCase = descriptionCase;
    }

      public Case( int userId, Date caseDate, String descriptionCase, String title){
        this.userId = userId;
        this.caseDate = caseDate;
        this.title = title;
        this.descriptionCase = descriptionCase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() { return userId; }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(Date caseDate) { this.caseDate = caseDate; }

    public String getDescriptionCase() {
        return descriptionCase;
    }

    public void setDescriptionCase(String descriptionCase) {
        this.descriptionCase = descriptionCase;
    }

    public String getTitle(){return title; }

    public void setTitle(String title){
        this.title = title;
    }
}
