package ru.planner.back;

import org.springframework.stereotype.Service;
import ru.planner.back.domain.Case;
import ru.planner.back.repos.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;

import java.sql.Date;
import java.util.List;

@Slf4j
@Service
public class DataBase {

    public DataBase(){
        log.info("info");
    }

    public DataBase(CaseRepository caseRepository1){
        log.info("info");
        caseRepository = caseRepository1;
    }


    @Autowired
    private CaseRepository caseRepository;

    public List<Case> getAll(){
        log.info("info");
        return caseRepository.findAll();
    }

    public void saveCase(int idUser, String text, String date, String title){
        Case aCase = new Case(idUser, Date.valueOf(date), text, title);
        caseRepository.save(aCase);
        log.info("info");
    }

    public void saveCase(Case aCase){
        caseRepository.save(aCase);
        log.info("info");
    }

    public void alterCase(String text, String date, String newText) {
        Case aCase;
        aCase = caseRepository.findByDescriptionCaseAndCaseDate(text, Date.valueOf(date));
        if (aCase != null){
            caseRepository.delete(aCase);
            aCase.setDescriptionCase(newText);
            caseRepository.save(aCase);
        }
        log.info("info");
    }

    public void alterCaseTitle(String text, String date, String newtitle) {
        Case aCase;
        aCase = caseRepository.findByDescriptionCaseAndCaseDate(text, Date.valueOf(date));
        if (aCase != null) {
            caseRepository.delete(aCase);
            aCase.setTitle(newtitle);
            caseRepository.save(aCase);
        }
        log.info("info");
    }

    public void deleteCase(String text, String date) {
        Case aCase;
        aCase = caseRepository.findByDescriptionCaseAndCaseDate(text, Date.valueOf(date));
        caseRepository.delete(aCase);
        log.info("info");
    }

    public void deleteCase(int id){
        caseRepository.deleteById(id);
        log.info("info");
    }

    public void setCaseRepository(CaseRepository caseRepository1){
        caseRepository =caseRepository1;
        log.info("info");
    }

}
