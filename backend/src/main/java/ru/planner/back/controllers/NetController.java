package ru.planner.back.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.planner.back.DataBase;
import ru.planner.back.domain.Case;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController("http://localhost:8090")
public class NetController {
    @Autowired
    private DataBase dataBase;

  public NetController(){}
    
    public NetController(DataBase dataBase){
        this.dataBase = dataBase;
    }

    @PostMapping(value = "/save")
    public String saveListOfCases(@RequestParam int idU, @RequestParam String d, @RequestParam String date, @RequestParam String title){
        this.dataBase.saveCase(idU, d, date, title);
        return "Save success";
    }

    @PostMapping(value = "/alterDesc")
    public String updateListOfCases(@RequestParam String text, @RequestParam String date, @RequestParam String newt){
         if (text.equals("")) {
            return "old description is empty";
        }
            dataBase.alterCase(text, date, newt);
            return "alter ok";
        }

    @PostMapping(value = "/alterTitle")
    public String updateListOfCasesTitle(@RequestParam String text, @RequestParam String date, @RequestParam String title){
        if (text.equals("")) {
            return "old title is empty";
        }
        dataBase.alterCaseTitle(text, date, title);
        return "alter ok";
    }

    @PostMapping(value = "/delete")
    public ResponseEntity<Case> deleteListOfCases(@RequestParam Integer taskId){
        this.dataBase.deleteCase(taskId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<Case>> getAllListOfCases(){
        List<Case> listsOfCases = this.dataBase.getAll();
      if (listsOfCases == null){
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        return new ResponseEntity<>(listsOfCases, HttpStatus.OK);
    }
}
